import os
import sys
import glob
import json
import torch.nn.functional as func
import numpy as np

import torch
import math

from torch.autograd import Variable

from model import RNNLM

embed_size = 32
hidden_size = 1024
num_layers = 3
num_epochs = 600
num_samples = 1000   # number of words to be sampled
batch_size = 128
seq_length = 50
learning_rate = 0.0012


def detach(states):
    return [state.detach() for state in states]


def sample(preds, temperature=1.0, beam_width=1):
    max_int = max(preds)
    preds = [x / max_int for x in preds]
    # helper function to sample an index from a probability array
    preds = [math.e**x / temperature for x in preds]
    preds = [x / sum(preds) for x in preds]
    return preds
    # return [x for x, y in sorted(enumerate(preds), key=lambda x: x[1])][-beam_width:][::-1]


def load_checkpoint(filename='final_model.pkl'):
    state = torch.load(filename)
    epoch = state['epoch']
    model_state = state['state_dict']
    optimizer = state['optimizer']
    return epoch, model_state, optimizer


directory = sys.argv[1]
if not os.path.exists('./' + directory):
    print("No such directory!")
    sys.exit()
else:
    os.chdir('./' + directory)

with open('config.json', 'r') as access:
    config = json.load(access)
if not (glob.glob('*.txt') or glob.glob('*.db')):
    print("No source file!")
    sys.exit()

if glob.glob('*.txt'):
    name = glob.glob('*.txt')[0]
    source = open(name, 'r').read()
    _batches = len(source) // batch_size
    source = source[:_batches * batch_size]
    chars = sorted(list(set(source)))
    char_to_int = {k: v for v, k in enumerate(chars)}
    int_to_char = {v: k for v, k in enumerate(chars)}
    ids = torch.LongTensor([char_to_int[x] for x in source]).view(batch_size,
                                                                  -1)
else:
    sys.exit()

vocab_size = len(char_to_int)
model = RNNLM(vocab_size, embed_size, hidden_size, num_layers)

if os.path.isfile('final_model.pkl'):
    start_epoch, model_state, optimizer_state = load_checkpoint()
    model.load_state_dict(model_state)


# model = RNNLM(vocab_size, embed_size, hidden_size, num_layers)
# model.load_state_dict(torch.load('model.pkl'))
print(model)

# Sampling
with open('./samples.txt', 'w') as f:
    # Set intial hidden ane memory states
    state = (Variable(torch.zeros(num_layers, 1, hidden_size)),
             Variable(torch.zeros(num_layers, 1, hidden_size)))

    # Select one word id randomly
    prob = torch.ones(vocab_size)
    input = Variable(torch.multinomial(prob, num_samples=1).unsqueeze(1),
                     volatile=True)

    for i in range(num_samples):
        # Forward propagate rnn
        print(input)
        output, state = model(input, state)
        # Sample a word id
        prob = output.squeeze().data
        # prob = func.softmax(torch.autograd.Variable(prob)).data
        word_id = int(np.argmax(sample(prob, 0.9)))
        # word_id = torch.multinomial(prob, 1)[0]

        # Feed sampled word id to next time step
        input.data.fill_(word_id)

        # File write
        word = int_to_char[word_id]
        f.write(word)

        detach(state)


        if (i+1) % 100 == 0:
            print('Sampled [%d/%d] words and save to %s'%(i+1,
                  num_samples, './samples.txt'))
    f.write('\n')
