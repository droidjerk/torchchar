import argparse
from collections import namedtuple
import glob
import json
import os
import numpy
import random
import string

import torch
import torch.nn
from PriorityQueue import PriorityQueue
from torch.autograd import Variable
from math import log10

import model


beam_state = namedtuple('BeamState', ['string', 'score', 'hidden_layers'])


class ModelSampler(object):
    def __init__(self, directory, model_settings, cuda):
        self.config = {}
        self.cuda = cuda
        self._check_directory(directory)
        self._load_config()
        self.prepare_text()

        model_type = model_settings['model']
        self.model = model.models[model_type](len(self.char_to_int),
                                              self.config['embed_size'],
                                              self.config['hidden_size'],
                                              self.config['num_layers'])
        self.load_model()
        self.temp = model_settings['temp']
        if cuda:
            self.model = self.model.cuda()
            self.criterion = self.criterion.cuda()
            self.config['cuda'] = True

    @staticmethod
    def _check_directory(dirpath):
        if not os.path.exists(dirpath):
            print("No such directory!")
            exit()
        else:
            os.chdir(dirpath)

    def _load_config(self):
        with open('config.json', 'r') as access:
            self.config = json.load(access)
        if not (glob.glob('*.txt') or glob.glob('*.db')):
            print("No configuration file!")
            exit()

    def load_model(self, model_dict=None):
        if not model_dict:
            model_dict = torch.load('final_model.pkl')
        self.model.load_state_dict(model_dict['state_dict'])
        return self.model

    @staticmethod
    def _detatch(states):
        return [state._detatch() for state in states]

    def prepare_text(self):
        name = glob.glob('*.txt')[0]
        source = open(name, 'r').read()
        print("Source size: {}".format(len(source)))

        chars = sorted(list(set(source)))
        self.char_to_int = {k: v for v, k in enumerate(chars)}
        self.int_to_char = {v: k for v, k in enumerate(chars)}

    def get_probs(self, inputs='\n', hidden=None):
        if not hidden:
            hidden = self.model.init_hidden(1, cuda=False)
        primer_input = Variable(torch.LongTensor([self.char_to_int[x] for x
                                                  in inputs]))

        for ind in primer_input:
            output, hidden = self.model(ind, hidden)
        output_dist = output.data.view(-1).div(self.temp).exp()
        return output_dist, hidden

    def sample_dist(self, output_dist):
        top_i = torch.multinomial(output_dist, 1)
        top_i = top_i[0]

        predicted_char = self.int_to_char[top_i]
        prob = (output_dist / sum(output_dist))[[top_i]]
        return predicted_char, prob

    def _expand_state_static(self, state):
        results = []
        probabilities, hidden_state = self.get_probs(inputs=state.string[-1],
                                                     hidden=state.hidden_layers)
        probabilities /= sum(probabilities)
        for index, prob in enumerate(probabilities):
            new_string = state.string + self.int_to_char[index]
            new_prob = prob - log10(prob)
            results.append(beam_state(new_string, new_prob, hidden_state))
        return results

    def _expand_state_nondeter(self, state):
        results = []
        probabilities, hidden_state = self.get_probs(inputs=state.string[-1],
                                                     hidden=state.hidden_layers)
        probabilities /= sum(probabilities)
        for index in torch.multinomial(probabilities, len(probabilities) // 2, replacement=False):
            new_string = state.string + self.int_to_char[index]
            new_prob = state.score - log10(min(probabilities[index], 2.0 / len(probabilities)))
            results.append(beam_state(new_string, new_prob, hidden_state))
        return results

    def sample(self, length):
        distribution, hidden = self.get_probs()
        last_output, _ = self.sample_dist(distribution)
        result = ''
        for _ in range(length):
            result += last_output
            distribution, hidden = self.get_probs(inputs=last_output,
                                                  hidden_layers=hidden)
            last_output, _ = self.sample_dist(distribution)
        return result

    def beam_sample(self, length=1000, beam_width=5):
        beam = PriorityQueue()
        start = numpy.random.choice(list(string.ascii_lowercase), (beam_width + 1, ))
        print(start)
        for element in start:
            beam.add_task(beam_state(element, 0, None), 0)
        for _ in range(length):
            states = beam.pop_n_tasks(beam_width)
            beam = PriorityQueue()
            for state in states:
                [beam.add_task(state, state.score) for state in self._expand_state_nondeter(state)]
        return beam.pop_task().string


def arguments():
    args = argparse.ArgumentParser()
    args.add_argument('-d', '--directory')
    args.add_argument('-s', '--samples', type=int, default=1000)
    args.add_argument('-b', '--beam', type=int, default=3)
    args.add_argument('-t', '--temp', type=float, default=1)
    args.add_argument('-c', '--cuda', action='store_true')
    args.add_argument('-m', '--model', choices=['lstm', 'gru'],
                      default='lstm')
    return args.parse_args()


if __name__ == '__main__':
    args = arguments()
    sample = ModelSampler(args.directory,
                          {'model': args.model,
                           'samples': args.samples,
                           'beam': args.beam,
                           'temp': args.temp},
                          args.cuda)
    print(sample.beam_sample(args.samples, args.beam))
