import argparse
import glob
import json
import os
import subprocess
from concurrent.futures import ThreadPoolExecutor

import numpy
import torch
import torch.nn
import tqdm
from torch.autograd import Variable

import model


class ModelTrain(object):
    def __init__(self, directory, model_settings, cuda):
        self.config = {}
        self.cuda = cuda
        self._check_directory(directory)
        self._load_config()
        self.prepare_text()

        model_type = model_settings['model']
        self.model = model.models[model_type](len(self.char_to_int),
                                              self.config['embed_size'],
                                              self.config['hidden_size'],
                                              self.config['num_layers'])
        self.criterion = torch.nn.CrossEntropyLoss()
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=0.0012)
        self.load_model()
        if cuda:
            import pdb;pdb.set_trace()
            self.model = self.model.cuda()
            self.criterion = self.criterion.cuda()
            self.config['cuda'] = True
            self._step = model_settings['step']

    @staticmethod
    def _check_directory(dirpath):
        if not os.path.exists(dirpath):
            print("No such directory!")
            exit()
        else:
            os.chdir(dirpath)

    def _load_config(self):
        with open('config.json', 'r') as access:
            self.config = json.load(access)
        if not (glob.glob('*.txt') or glob.glob('*.db')):
            print("No configuration file!")
            exit()

    def save_model(self, epoch, loss, fname='checkpoint{:04d}.pkl'):
        current_state = {
            'epoch': epoch,
            'loss': loss,
            'state_dict': self.model.state_dict(),
            'optimizer': self.optimizer.state_dict()
        }
        fname = fname.format(epoch)
        torch.save(current_state, fname)
        try:
            os.remove('final_model.pkl')
        except FileNotFoundError:
            pass
        subprocess.call(['ln', fname, 'final_model.pkl'])

    def load_model(self, model_dict=None):
        if not model_dict:
            if not os.path.exists('./final_model.pkl'):
                return
            model_dict = torch.load('final_model.pkl')
        self.model.load_state_dict(model_dict['state_dict'])
        self.optimizer.load_state_dict(model_dict['optimizer'])

    def _one_pass(self, start, end):
        hidden = self.model.init_hidden(self.config['batches'], self.cuda)
        loss = 0
        inps = Variable(self.ids[:, start:end + 1])
        if self.cuda:
            inps = inps.cuda()

        self.model.zero_grad()
        for char_index in range(end - start):
            output, hidden = self.model(inps[:, char_index], hidden)
            loss += self.criterion(output.view(self.config['batches'], -1),
                                   inps[:, char_index + 1])
        loss.backward()
        torch.nn.utils.clip_grad_norm(self.model.parameters(), 0.5)
        self.optimizer.step()
        return loss.data[0]  # / (end - start)

    def train(self):
        loss_avg = 0
        exectuor = ThreadPoolExecutor(max_workers=5)

        for epoch in tqdm.tqdm(range(0, self.config['num_epochs'])):
            start = numpy.random.choice(range(len(self.ids[0]) - self.config['seq_length']))
            loss = self._one_pass(start, start + self.config['seq_length'])
            loss_avg += loss

            if epoch % self._step == 0 and epoch > 0:
                exectuor.submit(self.save_model, epoch, loss)
                print('Epoch: {}, Loss: {:.5f}'.format(epoch, loss_avg / epoch))

    @staticmethod
    def _detatch(states):
        return [state._detatch() for state in states]

    def prepare_text(self):
        name = glob.glob('*.txt')[0]
        source = open(name, 'r').read()
        _batches = self.config['batches']
        _batch_size = len(source) // _batches
        print("Source size: {}".format(len(source)))

        chars = sorted(list(set(source)))
        self.char_to_int = {k: v for v, k in enumerate(chars)}
        self.int_to_char = {v: k for v, k in enumerate(chars)}

        self.ids = torch.LongTensor(_batches, _batch_size)
        print('Batches: {} Batch size:{}'.format(_batches, _batch_size))
        # if self.cuda:
        #     self.ids = self.ids.cuda()
        source = numpy.array([self.char_to_int[letter] for letter in source])
        index = 0
        for batch in range(_batches):
            self.ids[batch] = torch.from_numpy(source[index: index + _batch_size])
            index += _batch_size


def arguments():
    args = argparse.ArgumentParser()
    args.add_argument('-d', '--directory')
    args.add_argument('-s', '--step', type=int, default=1000)
    args.add_argument('-c', '--cuda', action='store_true')
    args.add_argument('-m', '--model', choices=['lstm', 'gru'],
                      default='lstm')
    return args.parse_args()


if __name__ == '__main__':
    args = arguments()
    train = ModelTrain(args.directory,
                       {'model': args.model,
                        'step': args.step},
                       args.cuda)
    train.train()
