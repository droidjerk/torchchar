import argparse
import glob
import json
import os
import shutil
import sys

import numpy as np
import torch
import torch.nn as nn
import tqdm
from torch.autograd import Variable

import model

# Hyper Parameters
learning_rate = 0.0012

best_loss = 100
prev_epoch = 0


def arguments():
    args = argparse.ArgumentParser()
    args.add_argument('-d', '--directory')
    return args.parse_args()


def save_checkpoint(current_state, current_best_loss,
                    fname='checkpoint{:03d}-{:01.5f}.pkl'):
    fname = fname.format(current_state['epoch'], current_state['loss'])
    print(fname)
    torch.save(current_state, fname)
    shutil.copyfile(fname, 'final_model.pkl')


def load_checkpoint(filename='final_model.pkl'):
    loaded_state = torch.load(filename)
    prev_epoch = loaded_state['epoch']
    prev_model_state = loaded_state['state_dict']
    prev_optimizer = loaded_state['optimizer']
    return prev_epoch, prev_model_state, prev_optimizer


directory = 
if not os.path.exists('./' + directory):
    print("No such directory!")
    sys.exit()
else:
    os.chdir('./' + directory)

with open('config.json', 'r') as access:
    config = json.load(access)
if not (glob.glob('*.txt') or glob.glob('*.db')):
    print("No source file!")
    sys.exit()

if glob.glob('*.txt'):
    name = glob.glob('*.txt')[0]
    source = open(name, 'r').read()
    _batches = config['batches']
    _batch_size = len(source) // _batches
    print("Source size: {}".format(len(source)))
    chars = sorted(list(set(source)))
    char_to_int = {k: v for v, k in enumerate(chars)}
    int_to_char = {v: k for v, k in enumerate(chars)}

    ids = torch.LongTensor(_batches, _batch_size)
    print('Batches: {} Batch size:{}'.format(_batches, _batch_size))

    index = 0
    for batch in range(_batches):
        for char in range(_batch_size):
            ids[batch][char] = char_to_int[source[index]]
            index += 1
    
    var_in = Variable(ids[:, :-1]).cuda()
    var_out = Variable(ids[:, 1:]).cuda()
else:
    sys.exit()

vocab_size = len(char_to_int)
length = config['seq_length']
start_epoch = 0

model = model.RNNLM(vocab_size, config['embed_size'], config['hidden_size'],
                    config['num_layers'])

model.cuda()

# Loss and Optimizer
criterion = nn.CrossEntropyLoss().cuda()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
if os.path.isfile('final_model.pkl'):
    start_epoch, model_state, optimizer_state = load_checkpoint()
    model.load_state_dict(model_state)
    optimizer.load_state_dict(optimizer_state)


# Truncated Backpropagation
def detach(all_states):
    return [state.detach() for state in all_states]


def train(start, end):
    hidden = model.init_hidden(config['batches'])
    loss = 0
    inps = var_in.detach().cuda()
    outs = var_out.detach().cuda()
    model.zero_grad()
    for char_index in range(start, end):
        inps = inps.detach()
        outs = outs.detach()

        output, hidden = model(inps[:, char_index], hidden)
        loss += criterion(output.view(config['batches'], -1), outs[:, char_index])
    loss.backward()
    torch.nn.utils.clip_grad_norm(model.parameters(), 0.5)
    optimizer.step()
    return loss.data[0] / (end - start)


def main():
    loss_avg = 0
    for epoch in tqdm.tqdm(range(prev_epoch, config['num_epochs'])):
        start = np.random.choice(range(len(ids)))
        loss = trai)n(start, start + config['seq_length'])
        loss_avg += loss

        if epoch % 50 == 0 and epoch > 0:
            save_checkpoint({
                'epoch': epoch + 1,
                'loss': loss_avg,
                'state_dict': model.state_dict(),
                'optimizer': optimizer.state_dict()
            }, loss_avg, fname='latest.pkl')
            print('Epoch: {}, Loss: {:.5f}'.format(epoch, loss / epoch))


if __name__ == "__main__":
    main()
