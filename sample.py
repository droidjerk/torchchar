import glob
import json
import math
import os
import random
import sys

import torch
from torch.autograd import Variable

import model

learning_rate = 0.0012


def sample(preds, temperature=1.0, beam_width=1):
    preds = preds[-1]
    max_int = max(preds)
    preds = [x / max_int for x in preds]
    # helper function to sample an index from a probability array
    preds = [math.e**x / temperature for x in preds]
    preds = [x / sum(preds) for x in preds]
    return preds
    # return [x for x, y in sorted(enumerate(preds), key=lambda x: x[1])][-beam_width:][::-1]


def load_checkpoint(filename='final_model.pkl'):
    state = torch.load(filename, map_location=lambda storage, loc: storage)
    model_state = state['state_dict']
    return model_state


directory = sys.argv[1]
if not os.path.exists('./' + directory):
    print("No such directory!")
    sys.exit()
else:
    os.chdir('./' + directory)

with open('config.json', 'r') as access:
    config = json.load(access)
if not (glob.glob('*.txt') or glob.glob('*.db')):
    print("No source file!")
    sys.exit()

if glob.glob('*.txt'):
    name = glob.glob('*.txt')[0]
    source = open(name, 'r').read()
    chars = sorted(list(set(source)))
    char_to_int = {k: v for v, k in enumerate(chars)}
    int_to_char = {v: k for v, k in enumerate(chars)}
else:
    sys.exit()

vocab_size = len(char_to_int)

model = model.RNNLSTM(vocab_size, config['embed_size'],
                    config['hidden_size'], config['num_layers'])

if os.path.isfile('final_model.pkl'):
    model_state = load_checkpoint()
    model.load_state_dict(model_state)


def expand(sequence):
    new_seq = sequence[-config['seq_length']:]
    while len(new_seq) < config['seq_length']:
        new_seq.insert(0, 0)
    return torch.LongTensor(new_seq)


def evaluate(primer='A', prediction_length=100, temp=1.0):
    hidden = model.init_hidden(1, cuda=False)
    primer_input = Variable(torch.LongTensor([char_to_int[x] for x in primer]))
    predicted = primer
    
    for ind in range((len(primer)) - 1):
        _, hidden = model(primer_input[ind], hidden)
    inp = primer_input[-1]

    for ind in range(prediction_length):
        output, hidden = model(inp, hidden)
        output_dist = output.data.view(-1).div(temp).exp()
        top_i = torch.multinomial(output_dist, 1)
        top_i = top_i[0]

        predicted_char = int_to_char[top_i]
        predicted += predicted_char
        inp = Variable(torch.LongTensor([top_i]))
    return predicted


with open('./../samples.txt', 'w') as f:
    # Set intial hidden ane memory states
    primer = random.choice(list(char_to_int.keys()))
    output = evaluate(primer, config['num_samples'], temp=0.95)
    f.write(output)
    f.write('\n')
