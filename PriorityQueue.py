import heapq
import itertools


class PriorityQueue:
    def __init__(self):
        self.pq = []                         # list of entries arranged in a heap
        self.entry_finder = {}               # mapping of tasks to entries
        self.REMOVED = '<self.REMOVED-task>'      # placeholder for a REMOVED task
        self.counter = itertools.count()     # unique sequence counter

    def add_task(self, task, priority=0):
        'Add a new task or update the priority of an existing task'
        if task in self.entry_finder:
            self.remove_task(task)
        count = next(self.counter)
        entry = [priority, count, task]
        self.entry_finder[task] = entry
        heapq.heappush(self.pq, entry)

    def remove_task(self, task):
        'Mark an existing task as REMOVED.  Raise KeyError if not found.'
        entry = self.entry_finder.pop(task)
        entry[-1] = self.REMOVED

    def pop_task(self):
        'Remove and return the lowest priority task. Raise KeyError if empty.'
        while self.pq:
            priority, count, task = heapq.heappop(self.pq)
            if task is not self.REMOVED:
                del self.entry_finder[task]
                return task
        raise KeyError('Pop from an empty priority queue')

    def pop_n_tasks(self, n):
        results = []
        while len(results) < n:
            results.append(self.pop_task())
        return results

    def look_at_first_priority(self):
        while self.pq:
            return (self.pq[0][0])
        raise KeyError('Empty priority queue')

    def __nonzero__(self):
        return (not self.empty())

    def empty(self):
        return len(self.pq) == 0

    def __contains__(self, value):
        return value in self.entry_finder
