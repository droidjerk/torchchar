import torch.nn as nn
from torch.autograd import Variable
import torch
import torch.nn.functional as F


class RNNLSTM(nn.Module):
    def __init__(self, vocab_size, embed_size, hidden_size, num_layers):
        super(RNNLSTM, self).__init__()
        self.embed = nn.Embedding(vocab_size, hidden_size)
        self.lstm = nn.LSTM(hidden_size, hidden_size, num_layers, dropout=0.2)
        self.linear = nn.Linear(hidden_size, vocab_size)
        self.init_weights()
        self.layers = num_layers
        self.hidden = hidden_size

    def init_weights(self):
        self.embed.weight.data.uniform_(-1, 1)
        self.linear.bias.data.fill_(0)
        self.linear.weight.data.uniform_(-1, 1)

    def forward(self, in_data, hidden):
        in_data = in_data.contiguous()
        batches = in_data.size(0)
        # Embed word ids to vectors
        in_data = self.embed(in_data.view(1, -1))
        # Forward propagate RNN
        out, hidden = self.lstm(in_data.view(1, batches, -1), hidden)
        # Reshape output to (batch_size*sequence_length, hidden_size)
        # Decode hidden states of all time step
        out = self.linear(out.view(batches, -1))
        return out, hidden

    def init_hidden(self, batches, cuda=True):
        if cuda:
            return (Variable(torch.zeros(self.layers, batches, self.hidden)).cuda(),
                    Variable(torch.zeros(self.layers, batches, self.hidden)).cuda())
        else:
            return (Variable(torch.zeros(self.layers, batches, self.hidden)),
                    Variable(torch.zeros(self.layers, batches, self.hidden)))


class RNNGRU(nn.Module):
    def __init__(self, vocab_size, embed_size, hidden_size, num_layers):
        super(RNNGRU, self).__init__()
        self.embed = nn.Embedding(vocab_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size, num_layers, dropout=0.2)
        self.linear = nn.Linear(hidden_size, vocab_size)
        self.init_weights()
        self.layers = num_layers
        self.hidden = hidden_size

    def init_weights(self):
        self.embed.weight.data.uniform_(-1, 1)
        self.linear.bias.data.fill_(0)
        self.linear.weight.data.uniform_(-1, 1)

    def forward(self, in_data, hidden):
        in_data = in_data.contiguous()
        batches = in_data.size(0)
        # Embed word ids to vectors
        in_data = self.embed(in_data.view(1, -1))
        # Forward propagate RNN
        out, hidden = self.gru(in_data.view(1, batches, -1), hidden)
        # Reshape output to (batch_size*sequence_length, hidden_size)
        # Decode hidden states of all time step
        out = self.linear(out.view(batches, -1))
        return out, hidden

    def init_hidden(self, batches, cuda=True):
        if cuda:
            return Variable(torch.zeros(self.layers, batches, self.hidden)).cuda()
        else:
            return Variable(torch.zeros(self.layers, batches, self.hidden))


models = {'gru': RNNGRU,
          'lstm': RNNLSTM}